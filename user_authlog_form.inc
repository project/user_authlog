<?php

/**
 * @file
 * Form functions for the user_authlog module.
 */


/**
 * Settings form.
 */
function user_authlog_form($form, &$form_state) {

  $form['#tree'] = TRUE;

  // User Authlog settings.
  $form['user_authlog_settings'] = array(
    '#title' => t('General settings'),
    '#type'  => 'fieldset',
    '#collapsible' => FALSE,
  );

  $default_values = variable_get('user_authlog_settings', array());
  $form['user_authlog_settings']['recipients'] = array(
    '#type' => 'textfield',
    '#title' => t('Email notification recipients:'),
    '#size' => 120,
    '#default_value' => isset($default_values['recipients']) ? $default_values['recipients'] : '',
    '#description' => t('Enter the email addresses that should be notified upon user login. Addresses must be comma separated.'),
  );

  // Per user tracking.
  $form['user_authlog_users'] = array(
    '#title' => t('Track by user (Recommended)'),
    '#type'  => 'fieldset',
    '#collapsible' => FALSE,
  );

  $default_values = variable_get('user_authlog_users', array());
  $form['user_authlog_users']['users'] = array(
    '#type' => 'textfield',
    '#title' => t('Users'),
    '#autocomplete_path' => 'autocomplete/users',
    '#default_value' => isset($default_values['users']) ? $default_values['users'] : '',
    '#description' => t('Enter a comma-separated list of users that will be tracked.'),
    '#element_validate' => array('user_authlog_valid_users'),
  );

  // Per-role tracking.
  $form['user_authlog_roles'] = array(
    '#title' => t('Track by role'),
    '#type'  => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
  );

  $role_options = array_map('check_plain', user_roles(TRUE));
  $default_values = variable_get('user_authlog_roles', array());
  $form['user_authlog_roles']['roles'] = array(
    '#type' => 'checkboxes',
    '#title' => t('User roles'),
    '#default_value' => isset($default_values['roles']) ? $default_values['roles'] : array(),
    '#options' => $role_options,
    '#description' => t('Track the selected role(s).'),
  );

  // Per-path visibility.
  $form['user_authlog_paths'] = array(
    '#title' => t('Pages to track'),
    '#type'  => 'fieldset',
    '#collapsible' => FALSE,
  );

  $options = array(
    USER_AUTHLOG_TRACK_PATH_NOTLISTED => t('Track all pages except those listed'),
    USER_AUTHLOG_TRACK_PATH_LISTED => t('Track only the listed pages'),
  );

  $description = t("Specify pages by using their paths. Enter one path per line. The '*' character is a wildcard. Example paths are %blog for the blog page and %blog-wildcard for every personal blog. %front is the front page.", array(
    '%blog'          => 'blog',
    '%blog-wildcard' => 'blog/*',
    '%front'         => '<front>',
  ));

  $default_values = variable_get('user_authlog_paths', array());

  $form['user_authlog_paths']['paths'] = array(
    '#type' => 'radios',
    '#options' => $options,
    '#default_value' => isset($default_values['paths']) ? $default_values['paths'] : 2,
  );

  $form['user_authlog_paths']['pages'] = array(
    '#type' => 'textarea',
    '#title' => '<span class="element-invisible">Page</span>',
    '#default_value' => isset($default_values['pages']) ? $default_values['pages'] : 'admin/*',
    '#description' => $description,
  );

  return system_settings_form($form);

}
