User Auth.log
-------------

-- SUMMARY --
Allows for logging user actions on sensitive/critical administrative pages.
Logging is limited to specific users and/or user roles (configurable). Logging
only occurs on specific administrative pages (configurable).

-- BACKGROUND --
This module was intended to help maintain stability/accountability on Drupal
projects with multiple technical administrators. For example, a company may have
a Drupal site builder “in-house”, but engage an outside firm for larger updates
and/or monthly maintenance. This module allows for logging the sensitive
activity of these advanced users (e.g. visiting specific settings pages) so that
in the event of technical problems, troubleshooting can take place more
expediently.

-- A WORD OF CAUTION --
This module is not intended to log more than a few low-use user accounts on any
given site. It should not be used to track regular site users. Even tracking
day-to-day content administrators may prove problematic for performance reasons
since all tracked actions are logged in the database. In most cases concerns
about user access to critical areas of the site should be mitigated by creating
specific user roles with a limited permissions model, and not by using this
module.

-- REQUIREMENTS --

None.

-- INSTALLATION --

See documentation on Drupal.org for information on how to install modules.
https://drupal.org/documentation/install/modules-themes/modules-7

-- CONFIGURATION --

After installing the module head over to the configuration page:
admin/config/people/user_authlog

-- CONTACT --

Maintainers:
Kirschbaum
enzipher


This project has been sponsored by Knectar <www.knectar.com>.
