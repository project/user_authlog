<?php

/**
 * @file
 * Email functions for the user_authlog module.
 */


/**
 * Implements hook_mail().
 */
function user_authlog_mail($key, &$message, $params) {

  global $user;
  global $base_url;
  $subject = $body = '';
  $site_name = variable_get('site_name', 'Drupal');

  // Language selection.
  $options = array(
    'langcode' => $message['language']->language,
  );

  switch ($key) {
    case "user_login":
      $message['subject'] = t('User @name logged in to @site-name', array(
        '@name' => $user->name,
        '@site-name' => $site_name,
      ), $options);
      $message['body'] = array(t('The user @name logged in to the website "@site-name" located at @base-path on @date', array(
        '@name' => $user->name,
        '@site-name' => $site_name,
        '@base-path' => $base_url,
        '@date' => date("F j, Y, g:i a"),
      ), $options));
      break;

    case "module_disable":
      $message['subject'] = t('User @name at @site-name disabled User Authlog', array(
        '@name' => $user->name,
        '@site-name' => $site_name,
      ), $options);
      $message['body'] = array(t('The user @name from the website "@site-name" located at @base-path disabled the User Authlog module on @date', array(
        '@name' => $user->name,
        '@site-name' => $site_name,
        '@base-path' => $base_url,
        '@date' => date("F j, Y, g:i a"),
      ), $options));
      break;
  }

}


/**
 * Send a mail based on $values.
 */
function user_authlog_mail_send($values = array()) {

  $module   = $values['module'];
  $key      = $values['key'];
  $to       = $values['to'];
  $from     = isset($values['from']) ? $values['from'] : variable_get('site_mail');
  $language = isset($values['lang']) ? $values['lang'] : language_default();

  $params = array(
    'subject' => $values['subject'],
    'body'    => $values['body'],
  );

  $send = TRUE;
  $mail = drupal_mail($module, $key, $to, $language, $params, $from, $send);

  if ($mail['result']) {
    drupal_set_message(t('A notification email has been sent to the system administrator.'), 'status');
    return TRUE;
  }
  else {
    watchdog('user_authlog', 'Failed to send the email.', array(), WATCHDOG_ALERT);
    return FALSE;
  }
}
